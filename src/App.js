import React, { useEffect, useState }  from "react";
import axios from "axios";

import  { DropDown } from './DropDown';
import { Grid } from './Grid';

import "./style.css";

export const App = () => {

  const apiUrl = 'http://localhost:2727/api';
  const [teams, setTeams] = useState({options: [] , handleSelect: null });
  const [users, setUsers] = useState({options: [], handleSelect: null});
  const [sales, setSales] = useState([]);

  useEffect(() => {
    getUsers();
    getTeams();
  }, []);

  const getUsers = async () => {
    try {
      const response = await axios.get(apiUrl + "/users");
      setUsers({ options: response.data.users , handleSelect: handleChangeUsers });
    } catch(error){
      alert("Ocurrio un error al leer los equipos");
    }
  }

  const getTeams = async () => {
    try {
      const response = await axios.get(apiUrl + "/teams");
      setTeams({ options: response.data.teams, handleSelect: handleChangeTeams });
    } catch(error){
      alert("Ocurrio un error al leer los equipos");
    }
  }

  const handleChangeUsers = async (event) => {
    try {
      const response = await axios.get(apiUrl + "/sales/users/" + event.target.value);
      setSales(response.data.sales);
    } catch(error){
      alert("Ocurrio un error al leer los equipos");
    }
  }

  const handleChangeTeams = async (event) => {
    try {
      const response = await axios.get(apiUrl + "/sales/team/" + event.target.value);
      setSales(response.data.sales);
    } catch(error){
      alert("Ocurrio un error al leer los equipos");
    }
  }

  return (
    <>
      <h3>VENTAS</h3>
      <h4>Usuarios</h4>
      <DropDown data = {users} />
      <h4>Teams</h4>
      <DropDown data = {teams} />
      <Grid sales = {sales}/>
    </>
  );
};