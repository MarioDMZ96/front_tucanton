import React, { useEffect, useState }  from "react";
import axios from "axios";

import "./style.css";

export const DropDown = (props) => {
    const { options, handleSelect } = props.data;

    return (
    <>
        {   
            options.length === 0 ? <h4>Cargando...</h4> : 
            <div className="caja">
                <select  onChange = { handleSelect } >
                    <option key = {0}>Selecciona</option>
                    {
                        options.map( item => {
                            return <option key = {item.id+1} value = {item.id}>{ item.name }</option>
                        })
                    }
                </select>
            </div>
        }
    </>
    );
};