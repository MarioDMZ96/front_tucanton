import React, { useEffect, useState }  from "react";
import axios from "axios";


import "./style.css";

export const Grid = (props) => {
    const sales = props.sales;
    return (
        <>
        <div className="grid">
                {
                    sales.length === 0 ? <h4>Cargando...</h4> :
                    sales.map( (item,index) => {
                        return <div key = {index} className="box">NOMBRE: {item.nombre} - VENTAS: {item.ventas_totales}</div>
                    })
                }
        </div>
        </>
    );
};